﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvoII.Entities
{
    [Serializable]
    class Extinguidor
    {
        private int id;
        private string categoria;
        private string marca;
        private int capacidad;
        private string tipo;
        private string undMedida;
        private int cantidad;

        public Extinguidor(int id, string categoria, string marca, int capacidad, string tipo, string undMedida, int cantidad)
        {
            this.Id = id;
            this.Categoria = categoria;
            this.Marca = marca;
            this.Capacidad = capacidad;
            this.Tipo = tipo;
            this.UndMedida = undMedida;
            this.Cantidad = cantidad;
        }

        public int Id { get => id; set => id = value; }
        public string Categoria { get => categoria; set => categoria = value; }
        public string Marca { get => marca; set => marca = value; }
        public int Capacidad { get => capacidad; set => capacidad = value; }
        public string Tipo { get => tipo; set => tipo = value; }
        public string UndMedida { get => undMedida; set => undMedida = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
    }
}
