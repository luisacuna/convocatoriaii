﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConvoII.Entities
{
    public partial class FrmExtinguidor : Form
    {
        private DataSet dsExtinguidor;
        private BindingSource bsExtinguidor;
        private DataTable dtExtinguidor;
        private DataRow drExtinguidor;
        private List<Extinguidor> extinguidores;
        private Extinguidor ext;
        private SerializeExtinguidor se;

        public FrmExtinguidor()
        {
            InitializeComponent();
            bsExtinguidor = new BindingSource();
            se = new SerializeExtinguidor();
        }

        internal List<Extinguidor> Extinguidores { get => extinguidores; set => extinguidores = value; }
        public DataSet DsExtinguidor { get => dsExtinguidor; set => dsExtinguidor = value; }
        public DataTable DtExtinguidor { get => dtExtinguidor; set => dtExtinguidor = value; }
        public DataRow DrExtinguidor
        {
            set
            {
                drExtinguidor = value;
                cmbCategoria.SelectedItem = drExtinguidor["Categoría"].ToString();
                cmbMarca.SelectedItem = drExtinguidor["Marca"].ToString();
                cmbCapacidad.SelectedItem = drExtinguidor["Capacidad"].ToString();
                cmbTipo.SelectedItem = drExtinguidor["Tipo"].ToString();
                cmbUndMedida.SelectedItem = drExtinguidor["Unidad"].ToString();
                nudCantidad.Value = (Int32)drExtinguidor["Cantidad"];
            }
        }


        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (cmbCategoria.SelectedItem == null)
            {
                MessageBox.Show(this, "El campo Categoria es obligatorio", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (cmbMarca.SelectedItem == null)
            {
                MessageBox.Show(this, "El campo" + " Marca " + "es obligatorio", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (cmbCapacidad.SelectedItem == null)
            {
                MessageBox.Show(this, "El campo Capacidad es obligatorio", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (cmbTipo.SelectedItem == null)
            {
                MessageBox.Show(this, "El campo Tipo es obligatorio", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (nudCantidad.Value == 0)
            {
                MessageBox.Show(this, "El campo Cantidad es obligatorio", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if ((cmbTipo.SelectedItem.ToString() == "H2O") && (cmbUndMedida.SelectedItem.ToString() == "Libras"))
            {
                MessageBox.Show(this, "Un extintor H2O no puede medirse en libras", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if ((cmbTipo.SelectedItem.ToString() == "CO2") && (cmbUndMedida.SelectedItem.ToString() == "Litros"))
            {
                MessageBox.Show(this, "Un extintor CO2 no puede medirse en litros", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if ((cmbTipo.SelectedItem.ToString() == "QS") && (cmbUndMedida.SelectedItem.ToString() == "Litros"))
            {
                MessageBox.Show(this, "Un extintor QS no puede medirse en litros", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            bool isNull = false;
            int confirmacion = 0;

            string categoria, marca, tipo, undMedida;
            int capacidad, cantidad;

            categoria = cmbCategoria.SelectedItem.ToString();
            marca = cmbMarca.SelectedItem.ToString();
            capacidad = Int32.Parse(cmbCapacidad.SelectedItem.ToString());
            tipo = cmbTipo.SelectedItem.ToString();
            undMedida = cmbUndMedida.SelectedItem.ToString();
            cantidad = (Int32)nudCantidad.Value;

            try
            {
                extinguidores = se.ExtinguidoresList();
            }
            catch
            {
                isNull = true;
            }

            if (drExtinguidor != null)
            {
                DataRow drNew = DtExtinguidor.NewRow();

                int index = DtExtinguidor.Rows.IndexOf(drExtinguidor);

                drNew["Id"] = drExtinguidor["Id"];
                drNew["Categoría"] = categoria;
                drNew["Marca"] = marca;
                drNew["Capacidad"] = capacidad;
                drNew["Tipo"] = tipo;
                drNew["Unidad"] = undMedida;
                drNew["Cantidad"] = cantidad;

                DtExtinguidor.Rows.RemoveAt(index);
                DtExtinguidor.Rows.InsertAt(drNew, index);

                for (int i = 0; i < extinguidores.Count; i++)
                {
                    if (extinguidores[i].Id.Equals(index + 1))
                    {
                        ext = new Extinguidor(i + 1, categoria, marca, capacidad, tipo, undMedida, cantidad);
                        confirmacion = se.Update(ext);
                        break;
                    }
                }

                if (confirmacion == 0)
                {
                    MessageBox.Show(this, "Editado con éxito", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                Dispose();
                return;

            }
            else
            {
                if (isNull == true)
                {
                    ext = new Extinguidor(1, categoria, marca, capacidad, tipo, undMedida, cantidad);
                    DtExtinguidor.Rows.Add(1, categoria, marca, capacidad, tipo, undMedida, cantidad);
                    confirmacion = se.Save(ext);
                }
                else
                {
                    Extinguidor last = extinguidores[extinguidores.Count - 1];

                    ext = new Extinguidor(last.Id + 1, categoria, marca, capacidad, tipo, undMedida, cantidad);
                    DtExtinguidor.Rows.Add(last.Id + 1, categoria, marca, capacidad, tipo, undMedida, cantidad);
                    confirmacion = se.Save(ext);
                }


                if (confirmacion == 1)
                {
                    MessageBox.Show(this, "El número de cédula ya existe en el registro", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (confirmacion == 0)
                {
                    MessageBox.Show(this, "Añadido correctamente", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            Dispose();
        }
    }
}
