﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConvoII.Entities
{
    public partial class FrmGestionExtinguidor : Form
    {
        private DataSet dsExtinguidor;
        private BindingSource bsExtinguidor;
        private DataTable dtExtinguidor;
        private List<Extinguidor> extinguidores = new List<Extinguidor>();
        private SerializeExtinguidor se;
        private Extinguidor ext;

        public FrmGestionExtinguidor()
        {
            InitializeComponent();
            bsExtinguidor = new BindingSource();
            se = new SerializeExtinguidor();
        }

        public DataSet DsExtinguidor { get => dsExtinguidor; set => dsExtinguidor = value; }

        private void FrmGestionExtinguidor_Load(object sender, EventArgs e)
        {
            bool isNull = false;

            dtExtinguidor = dsExtinguidor.Tables["Extinguidores"];
            try
            {
                extinguidores = se.ExtinguidoresList();
            }
            catch
            {
                isNull = true;
            }

            if (isNull == true)
            {
                MessageBox.Show(this, "El registro está vacío, agregue uno nuevo", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                dtExtinguidor.Rows.Clear();
                foreach (Extinguidor emp in extinguidores)
                {
                    dtExtinguidor.Rows.Add(emp.Id, emp.Categoria, emp.Marca, emp.Capacidad, emp. Tipo, emp.UndMedida, emp.Cantidad);
                }
            }

            bsExtinguidor.DataSource = DsExtinguidor;
            bsExtinguidor.DataMember = DsExtinguidor.Tables["Extinguidores"].TableName;
            dgvExtinguidor.DataSource = bsExtinguidor;
            dgvExtinguidor.AutoGenerateColumns = true;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmExtinguidor fe = new FrmExtinguidor();
            fe.DsExtinguidor = DsExtinguidor;
            fe.DtExtinguidor = DsExtinguidor.Tables["Extinguidores"];
            fe.Extinguidores = extinguidores;
            fe.ShowDialog();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvExtinguidor.SelectedRows;

            if (dgvExtinguidor.Rows.Count == 1)
            {
                MessageBox.Show(this, "No puede editar, no existe ningún registro", "Mensaje del Sitema", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            FrmExtinguidor fp = new FrmExtinguidor();
            fp.DtExtinguidor = DsExtinguidor.Tables["Extinguidores"];
            fp.DsExtinguidor = DsExtinguidor;
            fp.DrExtinguidor = drow;
            fp.ShowDialog();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string categoria = "", marca = "", tipo = "", undMedida = "";
            int capacidad = 0, cantidad = 0;
            int confirmacion = 0;

            DataGridViewSelectedRowCollection rowCollection = dgvExtinguidor.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;


            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                int id = Int32.Parse(drow["Id"].ToString());
                categoria = drow["Categoría"].ToString();
                marca = drow["Marca"].ToString();
                capacidad = Int32.Parse(drow["Capacidad"].ToString());
                tipo = drow["Tipo"].ToString();
                undMedida = drow["Unidad"].ToString();
                cantidad = Int32.Parse(drow["Cantidad"].ToString());

                extinguidores = se.ExtinguidoresList();

                for (int i = 0; i < extinguidores.Count; i++)
                {
                    if (extinguidores[i].Id.Equals(id))
                    {
                        ext = new Extinguidor(i + 1, categoria, marca, capacidad, tipo, undMedida, cantidad);
                        confirmacion = se.Delete(ext);
                        break;
                    }
                }

                if (confirmacion == 0)
                {
                    DsExtinguidor.Tables["Extinguidores"].Rows.Remove(drow);

                    MessageBox.Show(this, "Registro eliminado satisfactoriamente. Porfavor recargue la tabla!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(this, "Error, no se ha podido eliminar el registro", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
