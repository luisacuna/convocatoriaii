﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ConvoII.Entities
{
    class SerializeExtinguidor
    {
        private List<Extinguidor> extinguidores = new List<Extinguidor>();
        private Stream st;
        private BinaryFormatter bf;

        //public SerializeEmployee()
        //{
        //    bf = new BinaryFormatter();
        //}

        public SerializeExtinguidor()
        {
            bf = new BinaryFormatter();
        }

        public int Save(Extinguidor extinguidor)
        {
            bool isNull = false;
            try
            {
                extinguidores = DeserializeExtinguidoresList();
            }
            catch (Exception)
            {
                isNull = true;
            }

            if (isNull == true)
            {
                extinguidores.Add(extinguidor);
                SerializeExtinguidoresList(extinguidores);

                return 0;
            }

            extinguidores.Add(extinguidor);
            SerializeExtinguidoresList(extinguidores);

            //0 = guardado exitosamente
            //1 = el Empleado ya existe

            return 0;
        }

        public int Update(Extinguidor extinguidor)
        {
            //bool isNull = false;
            //try
            //{
            extinguidores = DeserializeExtinguidoresList();
            //}
            //catch (Exception)
            //{
            //    isNull = true;
            //}

            extinguidores.RemoveAt(extinguidor.Id - 1);
            extinguidores.Insert(extinguidor.Id - 1, extinguidor);

            SerializeExtinguidoresList(extinguidores);

            return 0;
        }

        public int Delete(Extinguidor extinguidor)
        {
            extinguidores = DeserializeExtinguidoresList();

            extinguidores.RemoveAt(extinguidor.Id - 1);

            //for (int i = 0; i < empleados.Count; i++)
            //{
            //    empleados[i].Id = i + 1;
            //}

            SerializeExtinguidoresList(extinguidores);

            return 0;

        }

        private List<Extinguidor> DeserializeExtinguidoresList()
        {
            List<Extinguidor> usuarios = new List<Extinguidor>();
            st = new FileStream("Extinguidores.dat", FileMode.Open, FileAccess.Read, FileShare.None);
            usuarios = (List<Extinguidor>)bf.Deserialize(st);

            st.Close();

            return usuarios;
        }

        private void SerializeExtinguidoresList(List<Extinguidor> extinguidores)
        {
            st = new FileStream("Extinguidores.dat", FileMode.Create, FileAccess.Write, FileShare.None);
            bf.Serialize(st, extinguidores);

            st.Close();
        }

        public List<Extinguidor> ExtinguidoresList()
        {
            List<Extinguidor> extinguidores = new List<Extinguidor>();

            extinguidores = DeserializeExtinguidoresList();

            return extinguidores;
        }
    }
}
