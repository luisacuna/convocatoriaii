﻿using ConvoII.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConvoII
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void extintoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionExtinguidor fge = new FrmGestionExtinguidor();
            fge.MdiParent = this;
            fge.DsExtinguidor = dsSistema;
            fge.Show();
        }
    }
}
